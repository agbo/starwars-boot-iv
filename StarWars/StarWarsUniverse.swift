//
//  StarWarsUniverse.swift
//  StarWars
//
//  Created by Fernando Rodríguez Romero on 1/23/17.
//  Copyright © 2017 Keepcoding. All rights reserved.
//

import UIKit

// Representa a un conjunto de personajes
class StarWarsUniverse {
    
    //MARK: Utility types
    typealias StarWarsArray = [StarWarsCharacter]
    typealias StarWarsDictionary = [StarWarsAffiliation : StarWarsArray]
    
    //MARK: - Properties
    fileprivate var dict : StarWarsDictionary = StarWarsDictionary()
    
    
    //MARK: - Initialization
    init(characters chars: StarWarsArray){
        
        // Asignamos afiliaciones y un StarWarsArray
        // vacío a cada una de ellas
        dict = makeEmptyAffiliations()
        
        // Nos pateamos el array de starwars
        // y asignamos según afiliación
        for each in chars{
            dict[each.affiliation]?.append(each)
        }
        
        // Ordenamos los personajes dentro de
        // cada afiliación
        for (affiliation, characters) in dict{
            dict[affiliation] = characters.sorted()
        }
    }
    
        
        
        //MARK: - Accessors
        var affiliationCount : Int{
            get{
                // Cuantas afiliaciones hay?
                return dict.count
            }
        }
        
        func characterCount(forAffiliation affiliation: StarWarsAffiliation) ->Int{
            
            guard let count = dict[affiliation]?.count else{
                return 0
            }
            return count
            
        }
        
        func character(atIndex index: Int,
                       forAffiliation affiliation: StarWarsAffiliation)
            -> StarWarsCharacter{
                
                // el personaje nº index de la afiliación affiliation
                let chars = dict[affiliation]!
                let char = chars[index]
                return char
                
        }
        
        func affiliationName(_ affiliation: StarWarsAffiliation) -> String{
            return affiliation.rawValue
        }
        
        
        
        //MARK: - Utils
        func makeEmptyAffiliations() -> StarWarsDictionary{
            
            var d = StarWarsDictionary()
            
            d[.rebelAlliance] = StarWarsArray()
            d[.galacticEmpire] = StarWarsArray()
            d[.firstOrder] = StarWarsArray()
            d[.jabbaCriminalEmpire] = StarWarsArray()
            
            return d
        }
    }
    










