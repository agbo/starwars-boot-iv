//
//  WikiViewController.swift
//  StarWars
//
//  Created by Fernando Rodríguez Romero on 1/24/17.
//  Copyright © 2017 Keepcoding. All rights reserved.
//

import UIKit

class WikiViewController: UIViewController{

    //MARK: - Properties
    @IBOutlet weak var browser: UIWebView!
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    var model : StarWarsCharacter
    
    //MARK: - Initialization
    init(model: StarWarsCharacter) {
        
        self.model = model
        
        super.init(nibName: nil,
                   bundle: nil)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
 
    //MARK: - Sync model -> view
    func syncViewWithModel(){
        
        // Creamos un URLRequest
        let req = URLRequest(url: model.url)
        
        // la cargamos en el browser
        browser.loadRequest(req)
    }
    
    //MARK: - View lifecycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        syncViewWithModel()
        browser.delegate = self
        
        // alta
        subscribe()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // baja
        unsubscribe()
        
    }
}


//MARK: - UIWebViewDelegate
extension WikiViewController: UIWebViewDelegate {
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        // Mostrar el spinner
        spinner.isHidden = false
        // espinearlo
        spinner.startAnimating()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
        spinner.isHidden = true
        spinner.stopAnimating()
    }
}

//MARK: - Notifications
extension WikiViewController{
    
    func subscribe(){
        
        // Ref. al NotificationCenter
        let nc = NotificationCenter.default
        
        nc.addObserver(forName: UniverseTableViewController.notificationName, object: nil, queue: OperationQueue.main) { (note: Notification) in
            
            // Extraigo el personaje de la notificación
            let userInfo = note.userInfo
            let char = userInfo?[UniverseTableViewController.characterKey]
            
            // cambio modelo
            self.model = char as! StarWarsCharacter
            
            // actualizo las vistas
            self.syncViewWithModel()
        }
    }
    
    func unsubscribe(){
        // Ref. al NotificationCenter
        let nc = NotificationCenter.default
        
        nc.removeObserver(self)
        
    }
}






